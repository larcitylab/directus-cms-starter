#!/bin/bash
SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -))
APP_PATH=$(realpath "${SCRIPT_PATH}/../")

cd ${APP_PATH}/directus

source ${APP_PATH}/directus/bin/00-compose-secrets-to-env.sh && docker-compose up