#!/bin/bash
SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -))
APP_PATH=$(realpath "${SCRIPT_PATH}/../")

cd ${APP_PATH}/

source ${APP_PATH}/bin/01-compose-secrets-to-env.sh && docker-compose build