#!/bin/bash
echo "Hostname: ${HOSTNAME}"

# Comparing hostname in lowercase
if [[ "${HOSTNAME,,}" =~ ^localadmins-mbp ]] || [[ "${HOSTNAME,,}" =~ ^lctduall01 ]]; then
  export API_EP="http://localhost:7700/_/"
else
  if [[ "${HOSTNAME,,}" =~ ho\.larcity\.org$ ]]; then
    export API_EP="http://api.stage.cms.ho.lar.city/_/"
  else
    export API_EP="http://cms-api-target/_/"
  fi
fi

echo "API ingress: ${API_EP}"

export DIRECTUS_CMS_API_ENDPOINT=$API_EP
