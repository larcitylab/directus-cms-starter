#!/bin/bash
SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -))
APP_PATH=$(realpath ${SCRIPT_PATH}/../)

echo "Script path: ${SCRIPT_PATH}"
echo "CMS app path: ${APP_PATH}"

. ${SCRIPT_PATH}/02-build.sh & docker-compose up