#!/bin/bash
# Comparing hostname in lowercase

UPPERCASE_TEST_STRING="${HOSTNAME^^}"
echo "Should be uppercase of ${UPPERCASE_TEST_STRING}? ${UPPERCASE_TEST_STRING,,}"

# Make sure the string itself is not changed
echo "Should still be uppercase: ${UPPERCASE_TEST_STRING}"

function regex_prefix_test() {
  # if  [[ "${HOSTNAME,,}" =~ ^lctduual01 ]]; then 
  #   echo "yes"
  # else
  #   echo "failed: regex test did not pass"
  # fi
  if  [[ "${HOSTNAME,,}" =~ ^${HOSTNAME,,} ]]; then 
    echo "yes"
  else
    echo "failed: regex test did not pass"
  fi
}
echo "Should print \"yes\" as result of regex_prefix_test: $(regex_prefix_test)"

function regex_starts_with_test() {
  TEST_STRING="Tom is a good boy"
  if [[ "${TEST_STRING,,}" =~ ^t ]]; then 
    echo "yes"
  else
    echo "failed: regex test did not pass"
  fi
}
echo "Should print \"yes\" as result of regex_starts_with_test: $(regex_starts_with_test)"

function regex_hostname_check() {
  if [[ "test.ho.larcity.org" =~ ho\.larcity\.org$ ]]; then 
    echo "yes"
  fi
}
echo <<EOF
should print "yes" as result of regex_hostname_check: $(regex_hostname_check)
EOF

# if [[ "${HOSTNAME,,}" =~ /^"localadmins\-mbp"/ ]] || [[ "${HOSTNAME,,}" =~ /"^lctduual01"/ ]]; then
#   export DIRECTUS_CMS_API_ENDPOINT="http://localhost:7700/_/"
# else
#   if [[ "${HOSTNAME,,}" =~ /ho.larcity.org$/ ]]; then
#     export DIRECTUS_CMS_API_ENDPOINT="http://api.stage.cms.ho.lar.city"
#   else
#     export DIRECTUS_CMS_API_ENDPOINT="http://cms-api-target/_/"
#   fi
# fi

# echo "API ingress: ${DIRECTUS_CMS_API_ENDPOINT}"