#!/bin/bash
SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -))
APP_PATH=$(realpath "${SCRIPT_PATH}/../")
SECRETS_PATH="${APP_PATH}/k8s/list-of-secrets"
CONFIG_PATH="${APP_PATH}/config"
DOCKER_ENV_CONFIG_FILE=${CONFIG_PATH}/docker.cfg

# # Ensure secrets path exists
# mkdir -pv ${SECRETS_PATH}

# Verify availability of secrets
if ! [ -d "${SECRETS_PATH}" ]; then
  echo "Invalid secrets path. You need to provision your deployment secrets here: ${SECRETS_PATH}"
  exit 2
fi

# Found secrets
echo "Admin secrets"
export CMS_ADMIN_EMAIL=$(cat ${SECRETS_PATH}/ADMIN_EMAIL.txt)
echo "username: $CMS_ADMIN_EMAIL"
export CMS_ADMIN_PASSWORD=$(cat ${SECRETS_PATH}/ADMIN_PASSWORD.txt)
echo "password: $CMS_ADMIN_PASSWORD"
echo ""
echo "DB secrets"
export CMS_MYSQL_ROOT_PASSWORD=$(cat ${SECRETS_PATH}/MYSQL_ROOT_PASSWORD.txt)
export CMS_MYSQL_USER=$(cat ${SECRETS_PATH}/MYSQL_USER.txt)
echo "username: $CMS_MYSQL_USER"
export CMS_MYSQL_PASSWORD=$(cat ${SECRETS_PATH}/MYSQL_PASSWORD.txt)
echo "password: $CMS_MYSQL_PASSWORD"
echo ""

# Figure out what endpoints should be
. ${SCRIPT_PATH}/00-orchestrate.sh

mkdir -pv $CONFIG_PATH

# Write environment variables
cat <<EOF >$DOCKER_ENV_CONFIG_FILE
# Directus MySQL Database
MYSQL_ROOT_PASSWORD="${CMS_MYSQL_ROOT_PASSWORD}"
MYSQL_USER="${CMS_MYSQL_USER}"
MYSQL_PASSWORD="${CMS_MYSQL_PASSWORD}"
# Directus CMS
CMS_ADMIN_EMAIL="${CMS_ADMIN_EMAIL}"
CMS_ADMIN_PASSWORD="${CMS_ADMIN_PASSWORD}"
CMS_MYSQL_ROOT_PASSWORD="${CMS_MYSQL_ROOT_PASSWORD}"
CMS_MYSQL_USER=${CMS_MYSQL_USER}
CMS_MYSQL_PASSWORD="${CMS_MYSQL_PASSWORD}"
# Container env variables
APP_TIMEZONE="America/New_York"
API_ENDPOINT="API; ${DIRECTUS_CMS_API_ENDPOINT}"
EOF

echo "Environment variables: $(cat ${DOCKER_ENV_CONFIG_FILE})"