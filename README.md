# LarCity Directus CMS

## Setting up the LAN docker registry

To begin accessing (and testing with) the Home Office LAN docker registry, you will need to add the following to your docker daemon config, typically located at `/etc/docker/daemon.json`:

```json
{
  "insecure-registries": ["registry.docker.ho.larcity.org:5000"]
}
```

## Setting up the Database

- A working image of the database has been baked @ `registry.docker.ho.larcity.org:5000/directus-mysql:1.0` and can be used for testing purposes

## How the Database Image Works

- The database image was build on mysql `5.7`
- Initial `mysqldump` files of a test version of the database were copied to `/docker-entrypoint-initdb.d` in the `Dockerfile`
- Mounting the `data` directory from the project root is an option that's also available so once the database is initialized, all of it's files are easily accessible (for backups, for example) from the host. **It's worth noting here that this setup is NOT intended for use in production - primarily concerns with scaling the app as well as the security issues with putting data inside containers for wider distribution**.

```dockerfile
# ./data/Dockerfile
FROM mysql:5.7

ENV DB_PATH /var/lib/mysql
ENV INIT_PATH /docker-entrypoint-initdb.d

VOLUME [ "/var/lib/mysql" ]

# Eg. side-load an existing Instance
# COPY ./mysql ${DB_PATH}
# RUN ls -la ${DB_PATH}

# Re-initialize
RUN ls -la ${INIT_PATH}
COPY ./directus-cms-db-20190217/ ${INIT_PATH}/
RUN ls -la ${INIT_PATH}
```
