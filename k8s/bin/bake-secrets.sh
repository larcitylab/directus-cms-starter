#!/bin/bash
APP_K8S_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -) | realpath "$(cat -)/../")
SECRETS_PATH="${APP_K8S_PATH}/list-of-secrets"
echo "App Kubernetes stuff path: ${APP_K8S_PATH}"
echo "Secrets path: ${SECRETS_PATH}"

# Verify availability of secrets
if ! [ -d "${SECRETS_PATH}" ]; then
  echo "Invalid secrets path. You need to provision your deployment secrets here: ${SECRETS_PATH}"
  exit 2
fi

# Found secrets path. Yay.

ADMIN_SECRET_FILE="${APP_K8S_PATH}/config/admin-secret.yml"
DB_ADMIN_SECRET_FILE="${APP_K8S_PATH}/config/db-admin-secret.yml"
DB_SECRET_FILE="${APP_K8S_PATH}/config/db-secret.yml"
if ! [ -f "$ADMIN_SECRET_FILE" ] || ! [ -f "$DB_SECRET_FILE" ]; then 
  echo "Invalid secrets file. You need to provision your deployment secrets files:"
  echo $ADMIN_SECRET_FILE
  echo $DB_SECRET_FILE
  exit 3
fi

# if ! [ -f "${SECRETS_PATH}/secrets" ]; then 
#   echo "Invalid secrets file. You need to provision your deployment secrets file here: ${SECRETS_PATH}/secrets"
#   echo 3
# fi

# # Attempt to create secrets
# kubectl create secret generic cms-db-secret --from-file=${SECRETS_PATH}/MYSQL_USER.txt \
#   --from-file=${SECRETS_PATH}/MYSQL_PASSWORD.txt &&\
# kubectl create secret generic cms-admin-secret --from-file=${SECRETS_PATH}/ADMIN_EMAIL.txt \
#   --from-file=${SECRETS_PATH}/ADMIN_PASSWORD.txt

echo "K8s context root:: $APP_K8S_PATH"
echo ""
echo "Definitions..."
echo "Admin secret definition: $ADMIN_SECRET_FILE"
echo "DB secret definition: $DB_SECRET_FILE"
echo ""

# kubectl create -f $ADMIN_SECRET_FILE &&\
# kubectl create -f $DB_SECRET_FILE
echo "Admin secrets (base64)"
export CMS_ADMIN_EMAIL=$(cat ${SECRETS_PATH}/ADMIN_EMAIL.txt | base64)
echo "username: $CMS_ADMIN_EMAIL"
export CMS_ADMIN_PASSWORD=$(cat ${SECRETS_PATH}/ADMIN_PASSWORD.txt | base64)
echo "password: $CMS_ADMIN_PASSWORD | base64)"
echo ""
echo "DB secrets (base64)"
export CMS_MYSQL_ROOT_PASSWORD=$(cat ${SECRETS_PATH}/MYSQL_ROOT_PASSWORD.txt | base64)
export CMS_MYSQL_USER=$(cat ${SECRETS_PATH}/MYSQL_USER.txt | base64)
echo "username: $CMS_MYSQL_USER"
export CMS_MYSQL_PASSWORD=$(cat ${SECRETS_PATH}/MYSQL_PASSWORD.txt | base64)
echo "password: $CMS_MYSQL_PASSWORD"

# Bake secrets 
echo "apiVersion: v1
kind: Secret
metadata:
  name: cms-admin-secret
type: Opaque
data:
  # ADMIN_EMAIL
  username: ${CMS_ADMIN_EMAIL}
  # ADMIN_PASSWORD
  password: ${CMS_ADMIN_PASSWORD}
" > ${ADMIN_SECRET_FILE}

echo "apiVersion: v1
kind: Secret
metadata:
  name: cms-db-admin-secret
type: Opaque
data:
  # MYSQL_PASSWORD
  password: ${CMS_MYSQL_ROOT_PASSWORD}
" > ${DB_ADMIN_SECRET_FILE}

echo "apiVersion: v1
kind: Secret
metadata:
  name: cms-db-secret
type: Opaque
data:
  # MYSQL_USER
  username: ${CMS_MYSQL_USER}
  # MYSQL_PASSWORD
  password: ${CMS_MYSQL_PASSWORD}
" > ${DB_SECRET_FILE}

# Clean secrets
kubectl delete -f $ADMIN_SECRET_FILE
kubectl delete -f $DB_ADMIN_SECRET_FILE
kubectl delete -f $DB_SECRET_FILE

# Attempt to create secrets
kubectl create -f $ADMIN_SECRET_FILE &&\
kubectl create -f $DB_ADMIN_SECRET_FILE &&\
kubectl create -f $DB_SECRET_FILE