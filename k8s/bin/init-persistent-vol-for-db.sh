#!/bin/bash
APP_K8S_PATH=$(realpath ${BASH_SOURCE[0]} | dirname $(cat -) | realpath "$(cat -)/../")
CMS_DB_HOST_PATH=$(echo "${APP_K8S_PATH}/../directus/k8s/mysql")
mkdir -pv $CMS_DB_HOST_PATH
[ -d "$CMS_DB_HOST_PATH" ] && export CMS_DB_HOST_PATH=$(realpath ${CMS_DB_HOST_PATH})

# Write Config File
echo "kind: PersistentVolume
apiVersion: v1
metadata:
  # name: mysql-pv-volume
  name: mysql-persistent-storage
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: \"${CMS_DB_HOST_PATH}\"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
" > ${APP_K8S_PATH}/config/01-deployment.mysql-persistent-volume.yml